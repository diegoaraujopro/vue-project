var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        title: 'See our products',
        products: [
            'Coffe',
            'Milkshake',
            'Popcorn',
            'Juice',
            'Soda',
            'Ketchup'
        ],
        cart: 0,
    },
    methods: {
        incrementCart() {
            this.cart += 1;
        },
        decrementCart() {
            if (this.cart > 0)
                this.cart -= 1;
        }
    }
})